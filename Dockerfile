FROM alpine:latest
LABEL maintainer "Damon <jiazhu3@cisco.com>"
ADD  ./tools/caddy   /
ADD ./docs/.vuepress/dist / 
CMD ["/caddy"]
